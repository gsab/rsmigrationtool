﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;

namespace RSBoronUpdateTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        UpdateLogic currentEvaluationLogic;

        private void btnEvaluate_Click(object sender, RoutedEventArgs e)
        {
            currentEvaluationLogic = new UpdateLogic(txtOldDirectory.Text, txtNewDirectory.Text);

            if (currentEvaluationLogic.EvaluateServerPaths())
            {
                btnMigrate.IsEnabled = true;
                grdFileList.ItemsSource = currentEvaluationLogic.FilesToCopy;
            }

        }

        private void btnMigrate_Click(object sender, RoutedEventArgs e)
        {
            currentEvaluationLogic.MigrateServer();
        }

        private void btnOldDirectory_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                var result = fbd.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK  && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtOldDirectory.Text = fbd.SelectedPath;
                }
            }
        }

        private void btnNewDirectory_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                var result = fbd.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtNewDirectory.Text = fbd.SelectedPath;
                }
            }
        }
    }
}
