﻿using RSServerLauncher;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;

namespace RSBoronUpdateTool
{
    public class UpdateLogic
    {
        private string OldServerPath { get; set; }
        private string NewServerPath { get; set; }

        public UpdateLogic(string oldServerPath, string newServerPath)
        {
            OldServerPath = oldServerPath;
            NewServerPath = newServerPath;
        }
        
        public bool MigrateServer()
        {
            string ErrorMessage = "";

            foreach(var file in FilesToCopy)
            {
                if(file.CopyFile)
                {
                    try
                    {
                        File.Copy(file.OriginalPath, file.DestinationPath);

                        if(file.FileType == CustomerFileType.InstallationConfiguration)
                        {
                            AddDefaultComponents(file.DestinationPath);
                        }

                    }
                    catch (Exception ex)
                    {
                        ErrorMessage += "Error while copying " + file.OriginalPath + " " + ex.Message+"\n";
                    }
                }
            }

            if(ErrorMessage != "")
            {
                MessageBox.Show(ErrorMessage);
                return false;
            }
            else
            {
                MessageBox.Show("All files copied succesfully!");
            }
            return true;
        }

        private void AddDefaultComponents(string destinationPath)
        {
            FileStream readString = null;
            SystemConfiguration currentConfiguration = null;
            try
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(SystemConfiguration));

                readString = new FileStream(destinationPath, FileMode.OpenOrCreate);

                currentConfiguration = (SystemConfiguration)(x.Deserialize(readString));
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if(readString != null)
                    readString.Close();
            }

            if(currentConfiguration != null)
            {
                var butlerComponentGuid = new Guid("9c9430d7-fa51-4126-b08e-2d6238e9c928");
                var httpApiComponentGuid = new Guid("22f96847-e204-4c65-ac7a-f753cc90f7be");

                if (!currentConfiguration.DataManagers.Contains(butlerComponentGuid))
                {
                    currentConfiguration.DataManagers.Add(butlerComponentGuid);
                }

                if (!currentConfiguration.DataManagers.Contains(httpApiComponentGuid))
                {
                    currentConfiguration.DataManagers.Add(httpApiComponentGuid);
                }

                FileStream writeString = new FileStream(destinationPath, FileMode.OpenOrCreate);

                try
                {

                    System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(SystemConfiguration));



                    x.Serialize(writeString, currentConfiguration);
                }
                catch
                {

                }
                finally
                {
                    writeString.Close();
                }

            }
        }

        public ObservableCollection<CustomerFile> FilesToCopy { get; } = new ObservableCollection<CustomerFile>();

        public bool EvaluateServerPaths()
        {
            //Check that the old serverPath contains an old server
            if (!CheckServerPath("Old server", OldServerPath, "Components", "Installations"))
            {
                return false;
            }

            //Check that the new serverPath contains a new server
            if (!CheckServerPath("New server", NewServerPath, "ServerVersions", "InstallationConfiguration"))
            {
                return false;
            }

            //Go thru Components and look for customer dlls
            AddCustomComponents();

            //Go thru ExternalDependencies and look for customer dlls
            AddExternalDependencies();

            //Go thru Resources/ThirdParty and look for customer dlls
            AddThirdPartyLibraries();

            //AddInstallationFile
            AddInstallationFile();

            return true;
        }

        private void AddInstallationFile()
        {
            try
            {
                var fileList = Directory.GetFiles(OldServerPath + "\\Installations");
                AddFilesToList(fileList.ToList(), CustomerFileType.InstallationConfiguration);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to get old installation configuration(s) - Aborting. Exception:\n" + ex.Message, "Error");
                Environment.Exit(-1);
            }            
        }

        private void AddThirdPartyLibraries()
        {
            try
            {
                RSServerLauncher.RSServerSelector.VersionFolder = NewServerPath + "\\ServerVersions\\";

                var serverVersionPath = RSServerLauncher.RSServerSelector.GetServerPath();

                var fileList = Directory.GetFiles(OldServerPath + "\\Resources\\ThirdParty");

                var newFileList = Directory.GetFiles(serverVersionPath + "\\Resources").Select(x => x.Replace(serverVersionPath + "\\Resources\\", "")).ToList();
                
                var filesToAdd = new List<string>();

                foreach (var file in fileList)
                {
                    if (newFileList.Any(x => file.Contains(x)) == false)
                        filesToAdd.Add(file);

                }
                AddFilesToList(filesToAdd, CustomerFileType.ThirdParty);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error when adding thirdparty files" + ex.Message);
            }
        }

        private void AddExternalDependencies()
        {
            try
            {
                var fileList = Directory.GetFiles(OldServerPath + "\\External dependencies");
                AddFilesToList(fileList.ToList(), CustomerFileType.ExternalDependencies);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to get external dependencies, please make sure this is expected. Exception:\n" + ex.Message, "Note");
            }
        }

        private void AddCustomComponents()
        {
            try
            {
                var fileList = Directory.GetFiles(OldServerPath + "\\Components");
                var unknownFiles = fileList.Where(x => !standardComponents.Any(y => x.Contains(y))).ToList();
                AddFilesToList(unknownFiles, CustomerFileType.Component);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to get custom components, please make sure this is expected. Exception:\n" + ex.Message, "Note");
            }
        }

        private void AddFilesToList(List<string> unknownFiles,CustomerFileType fileType)
        {
            var newPath = NewServerPath;
            var pathToRemove = "";

            if (fileType == CustomerFileType.Component)
            {
                newPath += "\\CustomComponents\\";
                pathToRemove = OldServerPath + "\\Components\\";
            }
            else if (fileType == CustomerFileType.ExternalDependencies)
            {
                newPath += "\\ExternalDependencies\\";
                pathToRemove = OldServerPath + "\\External dependencies\\";
            }
            else if (fileType == CustomerFileType.InstallationConfiguration)
            {
                newPath += "\\InstallationConfiguration\\";
                pathToRemove = OldServerPath + "\\Installations\\";
            }
            else if (fileType == CustomerFileType.ThirdParty)
            {
                newPath += "\\ExternalDependencies\\";
                pathToRemove = OldServerPath + "\\Resources\\ThirdParty\\";
            }

            foreach (var file in unknownFiles)
            {
                FilesToCopy.Add(new CustomerFile(file, file.Replace(pathToRemove, newPath), fileType));
            }
        }

        private string[] standardComponents = new string[]{"Component.ADUserManager", "Component.Aggregation", "Component.HTTPJsonAPI", "Component.ButlerService", "Component.ClientTimeSeriesService", "Component.DefinitionManager", "Component.FileBasedDataManager", "Component.IOInterface", "Component.Link", "Component.MasterVersion", "Component.PythonOrderIntegration", "Component.RSDataManager", "Component.RSPluginManager", "Component.RSServerSyncClient", "Component.RSUserManager", "Component.ServerClient", "Component.SMSGateway", "Component.SMTPGateway", "Component.SQLServerTimeSeriesService", "Component.WebProxy"};

        private bool CheckServerPath(string serverType, string serverDirectoryString, string folder1, string folder2)
        {
            try
            {
                var ServerDirectory = Directory.GetDirectories(serverDirectoryString);
                if (!ServerDirectory.Any(x => x.Contains(folder1)) || !ServerDirectory.Any(x => x.Contains(folder2)))
                {
                    MessageBox.Show(serverType + " path does not contain \"" + folder1 + "\" or \"" + folder2 + "\" folder.", "Error");
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while opening " + serverType + " path.\n" + ex.Message, "Error");
                return false;
            }

            return true;
        }
    }

    public class CustomerFile
    {
        
        public CustomerFile(string originalPath, string newPath, CustomerFileType Type)
        {
            OriginalPath = originalPath;
            DestinationPath = newPath;
            FileType = Type;
            CopyFile = true;
        }

        public bool CopyFile { get; set; }
        public string OriginalPath { get; set; }
        public CustomerFileType FileType { get; set; }
        public string DestinationPath { get; set; }
    }

    public enum CustomerFileType
    {
        Component = 1,
        ExternalDependencies = 2,
        InstallationConfiguration = 3,
        ThirdParty = 4
    }
}
