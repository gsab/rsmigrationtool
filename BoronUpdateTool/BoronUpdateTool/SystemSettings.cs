﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSServerLauncher
{
    public class SystemConfiguration
    {
        public long InstallationID { get; set; }

        public Guid CacheManager { get; set; }

        public Guid UserManager { get; set; }

        public Guid DataStore { get; set; }

        public Guid DefaultDataManager { get; set; }

        public Guid PreferedPluginManager { get; set; }

        public List<Guid> DataManagers { get; set; }

        public List<Guid> PluginManagers { get; set; }

        public List<ComponentSetting> ComponentSettings { get; set; }

    }

    public static class GlobalSettings
    {
        public static Guid InstallationIDSetting = new Guid("a2d08181-31d8-4809-b5ca-4e0e0a5d835a");
    }

    public class ComponentSetting
    {
        public Guid Id { get; set; }

        public Guid ComponentID { get; set; }

        public string SettingName { get; set; }

        public string Value { get; set; }
    }

}
