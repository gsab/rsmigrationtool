﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RSServerLauncher
{
    public static class RSServerSelector
    {
        public static string VersionFolder = "ServerVersions\\";
        public static string VersionFile = "versions.txt";
        public static string GetServerPath()
        {
            //fetch version list
            var versions = GetVersionHistory(VersionFolder + VersionFile);

            //get the last active version
            var activeVersion = GetActiveVersion(versions);

            if (activeVersion == null)
            {
                throw new Exception("Couldn't find any valid version in " + VersionFolder + VersionFile);
            }

            //return path
            return VersionFolder + activeVersion;
        }

        public static List<string> GetVersionHistory(string file)
        {
            try
            {
                return File.ReadAllLines(file).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to open file " + file + " with exception:\n" + ex.ToString());
            }            
        }


        /// <summary>
        /// Goes through an array of version paths in reverse (last to first) and selects the first one that seem to exist on the file system
        /// </summary>
        /// <param name="versionHistory">List of paths to different versions in ascending order (the last one should be the active version)</param>
        /// <returns>A sanity checked path to the latest active version. Null if it could not be found</returns>
        public static string GetActiveVersion(List<string> versionHistory)
        {
            foreach (var version in versionHistory.Where(x => x.Trim().Length >= 1 && !x.Trim().StartsWith("#") && x.Trim().IndexOfAny(Path.GetInvalidPathChars()) < 0).Reverse())
            {
                var trimmedVersion = version.Trim();
                //sanity check path 
                //sanity check path to this version
                if (Directory.Exists(VersionFolder + trimmedVersion))
                {
                    return trimmedVersion;
                }
            }

            return null;
        }

    }
}

